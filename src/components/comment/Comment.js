import React, { Component } from 'react';
import './Comment.css';
import {getDate} from '../../utils/helpers'
// import logo from '../../assets/img/logo.png';
import ReactMarkdown from 'react-markdown'
class Comment extends Component {
  render() {
    const {data}= this.props
    return (
        <div className='comment-wrapper'>
            
            <div className='user-image'>
              <img src={data.user && data.user.avatar_url} alt='user avatar' />
            </div>
            
            <div className='comment-data'>
              <div className='user-name'>
                <b>{data.user && data.user.login} </b>
                commented On {getDate(data.created_at)}
              </div>
              <div className='body'><ReactMarkdown source={data.body} /></div>
            </div>

        </div>
    )
  }
}

export default Comment;
