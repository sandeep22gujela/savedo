import React, { Component } from 'react';
import './Issue-Item.css'
import { getDate } from '../../utils/helpers';
import Octicon, { IssueOpened, Comment } from '@githubprimer/octicons-react'
class IssueItem extends Component {

  getLabels() {
    const { issue } = this.props
    var labels = [];
    console.log('issuelabe', issue.labels)
    if (!issue.labels)
      return
    labels = issue.labels.map((label, index) => {
      const style = { backgroundColor: '#' + label.color }
      return <span key={'label-' + index} className='label-component' style={style}>{label.name}</span>
    })
    return labels
  }
  render() {
    const { issue } = this.props
    return (
      <li className='issues-item-container'>

        <div className='icon-wrapper'>
          <Octicon icon={IssueOpened} size='small' verticalAlign='middle' />
        </div>


        <div className='title-wrapper'>
          <div className='title'>{issue.title}
            {this.getLabels()}
          </div>

          <div className='meta-wrapper'>
            <div className='issue-number'>#{issue.issueNumber} </div>
            <div className='ml-5'>Opened on {getDate(issue.createdAt || '')}</div>
            <div className='ml-5'>By {issue.userName} </div>
          </div>
        </div>

        <div className='comments'>
          {issue.comments > 0 ?
            <span>
              <Octicon icon={Comment} size='small' verticalAlign='middle' />  {issue.comments}
            </span>
            : null}
        </div>

      </li>
    )
  }
}

export default IssueItem;
