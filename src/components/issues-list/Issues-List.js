import React, { Component } from 'react';
import './Issues-List.css';
import {APP_URL} from '../../utils/constants'
import IssueItem from '../issue-item/Issue-Item'
import PropTypes from 'prop-types';
import Octicon, { Repo } from '@githubprimer/octicons-react'


class IssuesList extends Component {

    getIssuesList() {
        const { issuesList } = this.props
        return issuesList.map((issue, index) => {
          return <IssueItem key={index} issue={issue} />
        })
    
      }
    
      getPageTitle(){
        const urlArray=APP_URL.split('/')
        return <div className='mt-20'>
          
            <span><Octicon icon={Repo} size='small' verticalAlign='middle' /></span>
            <span className=' ml-5 project-title color-primary'>{`${urlArray[4]}/`}<b>{`${urlArray[5]}`}</b></span>
          </div>
      }
    
      render() {
        return (
          <div className='issues-list-container'>
            {this.getPageTitle()}
            <ul className='issues-list'>
              {this.getIssuesList()}
            </ul>
          </div>
        )
      }
}

IssuesList.propTypes={
  issuesList:PropTypes.array.isRequired
}

export default IssuesList;
