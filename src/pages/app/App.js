import React, { Component } from 'react';
import './App.css';
import Header from '../../components/header/Header'
import IssuesPage from '../../pages/issues-page/Issues-page'
import IssueDetailPage from '../../pages/issue-detail/Issue-detail-page'


class App extends Component {
  render() {
    return <div>
      <Header ></Header>
      {/* <IssuesPage></IssuesPage> */}
      <IssueDetailPage></IssueDetailPage>
    </div>
  }
}

export default App;
