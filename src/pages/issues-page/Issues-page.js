import React, { Component } from 'react';
import './Issues-page.css';
import {APP_URL} from '../../utils/constants'
import {jsonParse,transformIssueList} from '../../utils/helpers'
import IssuesList from '../../components/issues-list/Issues-List'

class IssuesPage extends Component {

    constructor(props, state) {
        super(props, state)
        this.state = {
            issuesList: []
        }
    }
    componentDidMount() {
        this.fetchIssuesList()
    }

    fetchIssuesList() {
        // Note: Not using any library for making Ajax request as just only one request is to be made.
        var that = this;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
                that.setState({
                    issuesList: jsonParse(xmlHttp.responseText)
                })
        }
        xmlHttp.open("GET", APP_URL+'issues');
        xmlHttp.setRequestHeader("Accept", "application/vnd.github.v3.text+json");
        xmlHttp.send();
    }

    render() {
        //toDo bring following code in to single line.
        const list = this.state.issuesList
        var issuesList
        issuesList = transformIssueList(list)
        return (
            <IssuesList issuesList={issuesList} />
        );
    }
}

export default IssuesPage;
