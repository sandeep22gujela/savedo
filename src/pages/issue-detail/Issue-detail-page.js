import React, { Component } from 'react';
import './Issue-detail-page.css';
import {APP_URL} from '../../utils/constants';
import {jsonParse, getDate} from '../../utils/helpers';
import Comment from '../../components/comment/Comment';
import Octicon, { IssueOpened } from '@githubprimer/octicons-react'


class IssueDetailPage extends Component {

    constructor(props, state) {
        super(props, state)
        this.state = {
            issueSummary: {},
            issueComments:[]
        }
    }
    componentDidMount() {
        this.fetchIssueSummary()
    }
    
    fetchIssueComments(){
        const {comments_url}=this.state.issueSummary
        if(comments_url){
            var that = this;
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
                that.setState({
                    issueComments: jsonParse(xmlHttp.responseText)
                })
            }
            //toDo make issue id dynamic from props
            xmlHttp.open("GET", comments_url);
            xmlHttp.setRequestHeader("Accept", "application/vnd.github.v3.symmetra-preview+json");
            xmlHttp.send();
        }
    }

    fetchIssueSummary() {
        // Note: Not using any library for making Ajax request as just only one request is to be made.
        var that = this;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
                that.setState({
                    issueSummary: jsonParse(xmlHttp.responseText)
                })
                that.fetchIssueComments()
        }
        //toDo make issue id dynamic from props
        xmlHttp.open("GET", APP_URL+'issues/14224');
        xmlHttp.setRequestHeader("Accept", "application/vnd.github.v3.symmetra-preview+json");
        xmlHttp.send();
    }

    getIssueComments(){
        const commentsData = this.state.issueComments;
        console.log('comments',commentsData)
        return this.state.issueComments.map((comment,index) => {
            return <Comment key={'comment-'+index} data={comment} />
        })
    }

    getTitle(){
        const issueSummary = this.state.issueSummary
        return <div>
                <span>
                    <h1 className='title'>{issueSummary.title} <span className='color-light'>#{issueSummary.number}</span></h1>
                </span>
            </div>
    }

    getSubtitle() {
        const issueSummary = this.state.issueSummary
        const userName= issueSummary.user && issueSummary.user.login

        return <div className='status-container'>     
            <div className='status-label-open'><Octicon icon={IssueOpened} size='small' verticalAlign='middle' /> &nbsp;Open</div>
            <div className='ml-5 color-light'><b>{userName}</b> opened this issue on {getDate(issueSummary.created_at)} . {issueSummary.comments} Comments</div>
        </div>
    }

    render() {
        //toDo bring following code in to single line.
        const issueSummary = this.state.issueSummary
        console.log('issue detail',issueSummary)
        return (
            <div className='issue-detail-container'>
                {this.getTitle()} 
                {this.getSubtitle()}
                {this.state.issueSummary ? <Comment data={this.state.issueSummary}  />:''}
                {this.getIssueComments()}
            </div>
        )
    }
}

export default IssueDetailPage;
