export const jsonParse = (text) => {
    var jsonData;
    try {
        jsonData = JSON.parse(text)
    }
    catch (e) {
        console.exception(e)
        return {}
    }
    return jsonData
}

export const transformIssueList = (issues)=> {
    if (issues.length === 0) {
        console.log('issues', issues)
        return []
    }

    var transformedIssueList = [];
    transformedIssueList = issues.map((data = {}) => {
        return {
            'comments': data.comments || 0 ,
            'createdAt': data.created_at || '',
            'issueNumber': data.number || null,
            'title': data.title || '',
            'userName': data.user.login || '',
            'labels':data.labels || []
        }

    })

    return transformedIssueList
}


export const getDate = (date) => {
    if (typeof date === 'string') {
        var jsDate = new Date(date);
        return ` ${jsDate.getDate()}/${jsDate.getMonth()}/${jsDate.getFullYear()}`
    }
    return 'sometime'
}